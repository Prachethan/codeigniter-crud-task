$(document).ready(function () {
    $('#users-table').hide();
    $('#skills').select2();

    // on load fetches the users list
    fetchAllUsers();



});

const addUser = () => {

    // Clears previous data
    $('#form-action').val('add');
    $('#skills').val('').trigger('change');
    $('#crop-box').attr('src', '');
    $('[name="name"]').val('');
    $('[name="user_id"]').val('');
    $('[name="email"]').val('');
    $('[name="dob"]').val('');

    setModalTitle('Add User');
    setBtnTitle('Add');

}
$(document).on('click', '#form-submit-btn', function (e) {
    e.preventDefault();
    // Edit Action or Add Action
    const action = $('#form-action').val();
    postUser(action);
});



const postUser = (action) => {
    let myForm = document.getElementById('user-form');
    let formData = new FormData(myForm);
    action = (action == 'add') ? 'add-user' : ('edit-user/' + $('#user_id').val());
    $.ajax({
        method: 'POST',
        data: formData,
        processData: false, // Using FormData, no need to process data.
        contentType: false,
        url: base_url + action,
        beforeSend: function () {
            $('#global-loader').toggleClass('is-active');
            $('#form-submit-btn').attr('disabled', true);
        },
        success: function (result) {
            $('#global-loader').toggleClass('is-active');
            // Closes the modal
            $('#modal-close').click();

            if (typeof result.success !== 'undefined' && result.success) {
                alert(result.message);

                fetchAllUsers();
            } else {
                alert('Something went wrong!!');
            }

            $('#form-submit-btn').removeAttr('disabled');




        },
        error: function () {
            $('#global-loader').toggleClass('is-active');

            $('#form-submit-btn').removeAttr('disabled');
        }
    });
};


const updateUser = () => {

}


// Fetches the users list
const fetchAllUsers = () => {


    $.ajax({
        method: 'POST',
        url: base_url + 'view-all-users',
        beforeSend: function () {
            $('#tbody').html('');
            $('#global-loader').toggleClass('is-active');
        },
        success: function (result) {
            $('#global-loader').toggleClass('is-active');

            if (typeof result !== 'undefined' && typeof result.users !== 'undefined' && result.users.length) {

                $('#users-table').show();
                $('#no-users-holder').hide();
                result.users.forEach((element, key) => {
                    let deleteTag = '<button class="btn btn-danger" onclick="handleDeleteUser(this);" data-id="' + btoa(element.id) + '"><i class="fa fa-trash"></i></button>';
                    let editTag = '<button class="btn btn-warnign" onclick="handleEditUser(this);" data-id="' + btoa(element.id) + '"><i class="fa fa-edit"></i></button>';
                    let userImage = (element.img === null || element.img === '')
                        ? 'https://via.placeholder.com/150?text=No%20Image'
                        : (base_url + 'uploads/users/' + element.img);


                    displayHtml = '<tr>' +
                        '<td>' + (key + 1) + '</td>' +
                        '<td>' + element.name + '</td>' +
                        '<td><img src="' + userImage + '" width="150" height="150"></td>' +
                        '<td>' + element.email + '</td>' +
                        '<td>' + editTag + '&nbsp;' + deleteTag + '</td>' +
                        +'</tr>';
                    $('#tbody').append(displayHtml);
                });

            } else {
                $('#no-users-holder').show();
                $('#users-table').hide;

            }


        },
        error: function () {
            $('#global-loader').toggleClass('is-active');

        }
    });
}

const handleDeleteUser = (ref) => {
    console.log('---Clicked on delete---');
    if (confirm('Are you sure?')) {
        const user_id = $(ref).data('id');
        deleteUser(user_id);
    }

}

const deleteUser = (user_id) => {
    $.ajax({
        method: 'POST',
        url: base_url + 'delete-user/' + atob(user_id),
        beforeSend: function () {
            $('#global-loader').toggleClass('is-active');

        },
        success: function (result) {
            $('#global-loader').toggleClass('is-active');

            if (typeof result.success !== 'undefined' && result.success) {
                alert(result.message);

                fetchAllUsers();
            } else {
                alert('Something went wrong!!');
            }
        },
        error: function () {
            $('#global-loader').toggleClass('is-active');

        }
    });
}

const handleEditUser = (ref) => {
    console.log('---Clicked on Edit---');
    const user_id = $(ref).data('id');
    editUser(user_id);
}

const editUser = (user_id) => {
    $.ajax({
        method: 'POST',
        url: base_url + 'view-user/' + atob(user_id),
        beforeSend: function () {
            $('#global-loader').toggleClass('is-active');

        },
        success: function (result) {
            $('#global-loader').toggleClass('is-active');
            if (typeof result !== 'undefined') {

                // Opens Edit Modal
                $('#add-user-modal').modal('show');

                // Changes the modal title
                setModalTitle('Edit User');
                setBtnTitle('Update');

                // set values of a each field in the form

                let name = result.user.name,
                    email = result.user.email,
                    gender = result.user.gender,
                    user_id = result.user.id,
                    dob = result.user.dob,
                    profile = result.user.img,
                    skills = (result.user.skills != null)
                        ? result.user.skills.split(',')
                        : [];



                $('[name="name"]').val(name);
                $('[name="user_id"]').val(user_id);
                $('[name="email"]').val(email);
                $('[name="dob"]').val(dob);
                $('[name="skills[]"]').val(skills).trigger('change');
                $('#crop-box').attr('src', (base_url + 'uploads/users/' + profile));
                initJcrop();


                $('[name="gender"]').filter(function () {
                    return $(this).val() == gender
                }).prop('checked', true);

                $('#form-action').val('edit');

            } else {
                alert('Something went wrong!!');
            }
        },
        error: function () {
            $('#global-loader').toggleClass('is-active');

        }
    });
}

const setModalTitle = (title) => $('[class="modal-title"]').text(title);
const setBtnTitle = (title) => $('#form-submit-btn').text(title);


/* jCrop */
const readURL = (input) => {
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            $('#crop-box')
                .attr('src', e.target.result)
                .width(500)
                .height(220);
            initJcrop();
        };
        reader.readAsDataURL(input.files[0]);
    }
}
const updateCoords = (c) => {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);

};

const initJcrop = () => {
    $('#crop-box').Jcrop({ // we linking Jcrop to our image with id=crop-box
        aspectRatio: 1,
        onChange: updateCoords,
        onSelect: updateCoords,
        boxWidth: 500,
        boxHeight: 220,
        minSize: [1, 1], // min crop size
        maxSize: [6000, 6000], // max crop size
        aspectRatio: 16 / 9, //keep aspect ratio
        setSelect: [0, 0, 250, 250],
        bgFade: true, // use fade effect
        bgOpacity: .3, // fade opacity
        allowSelect: false,
    });
}