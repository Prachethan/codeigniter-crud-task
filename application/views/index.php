   <!DOCTYPE html>
   <html lang="en">

   <head>
   	<title>Codeigniter Crud By PHP Code Builder</title>
   	<meta charset="utf-8">
   	<meta name="viewport" content="width=device-width, initial-scale=1">
   	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
   </head>

   <body>
   	<nav class="navbar navbar-inverse">
   		<div class="container-fluid">
   			<div class="navbar-header">
   				<a class="navbar-brand" href="http://crudegenerator.in">Codeigniter Crud By PHP Code Builder</a>
   			</div>
   			<ul class="nav navbar-nav">
   				<li class="active"><a href="<?php echo site_url(); ?>manage-users">Manage Users</a></li>
   				<li><a href="<?php echo site_url(); ?>add-users">Add Users</a></li>
   			</ul>
   		</div>
   	</nav>
   	<div class="container">
   		<h2>Manage Users</h2>
   		<?php if ($this->session->flashdata('success')) { ?>
   			<div class="alert alert-success">
   				<strong><span class="glyphicon glyphicon-ok"></span> <?php echo $this->session->flashdata('success'); ?></strong>
   			</div>
   		<?php } ?>

   		<?php if (!empty($userss)) { ?>
   			<table class="table table-hover">
   				<thead>
   					<tr>
   						<th>SL No</th>
   						<th>name</th>
   						<th>Actions</th>
   					</tr>
   				</thead>
   				<tbody>
   					<?php $i = 1;
						foreach ($userss as $users) { ?>
   						<tr>
   							<td> <?php echo $i; ?> </td>
   							<td> <a href="<?php echo site_url() ?>view-users/<?php echo $users->id ?>"> <?php echo $users->name ?> </a> </td>

   							<td>
   								<a href="<?php echo site_url() ?>change-status-users/<?php echo $users->id ?>"> <?php if ($users->status == 0) {
																														echo "Activate";
																													} else {
																														echo "Deactivate";
																													} ?></a>
   								<a href="<?php echo site_url() ?>edit-users/<?php echo $users->id ?>">Edit</a>
   								<a href="<?php echo site_url() ?>delete-users/<?php echo $users->id ?>" onclick="return confirm('are you sure to delete')">Delete</a>
   							</td>

   						</tr>
   					<?php $i++;
						} ?>
   				</tbody>
   			</table>
   		<?php } else { ?>
   			<div class="alert alert-info" role="alert">
   				<strong>No Userss Found!</strong>
   			</div>
   		<?php } ?>
   	</div>

   </body>

   </html>
   view-users.php

   <!DOCTYPE html>
   <html lang="en">

   <head>
   	<title>Codeigniter Crud By PHP Code Builder</title>
   	<meta charset="utf-8">
   	<meta name="viewport" content="width=device-width, initial-scale=1">
   	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
   </head>

   <body>

   	<nav class="navbar navbar-inverse">
   		<div class="container-fluid">
   			<div class="navbar-header">
   				<a class="navbar-brand" href="http://crudegenerator.in">Codeigniter Crud By PHP Code Builder</a>
   			</div>
   			<ul class="nav navbar-nav">
   				<li><a href="<?php echo site_url(); ?>manage-users">Manage Users</a></li>
   				<li><a href="<?php echo site_url(); ?>add-users">Add Users</a></li>
   			</ul>
   		</div>
   	</nav>

   	<div class="container">

   		<div class="row">
   			<div class="col-xs-12 col-md-10 well">
   				name : <?php echo $users[0]->name ?>
   			</div>
   		</div>
   		<div class="row">
   			<div class="col-xs-12 col-md-10 well">
   				email : <?php echo $users[0]->email ?>
   			</div>
   		</div>
   		<div class="row">
   			<div class="col-xs-12 col-md-10 well">
   				skills : <?php echo $users[0]->skills ?>
   			</div>
   		</div>
   		<div class="row">
   			<div class="col-xs-12 col-md-10 well">
   				gender : <?php echo $users[0]->gender ?>
   			</div>
   		</div>
   		<div class="row">
   			<div class="col-xs-12 col-md-10 well">
   				dob : <?php echo $users[0]->dob ?>
   			</div>
   		</div>

   	</div>

   </body>

   </html>