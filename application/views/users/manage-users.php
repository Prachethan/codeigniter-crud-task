<!DOCTYPE html>
<html lang="en">

<head>
    <title>Codeigniter Crud</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <!-- jCrop -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/0.9.15/css/jquery.Jcrop.css" integrity="sha512-G0VfAHKo2MoitbJamARbi9tuuv5rb+fuzVY+xMU1Z0t1/VkyVVAf9foAHGuSkymgMcj9XBn9bOB3BHsbgqbMvg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/0.9.15/js/jquery.Jcrop.js" integrity="sha512-8SpT7ueuEcyaOfE5XTafnVw9V3Bqz6uFzR3xYQIxWOed2ic4t6bfpL/k2JciMdML3n0k4QRZEe3EBFw+/eVLQA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <!-- FA -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <!-- Custom JS  -->
    <script src="<?php echo base_url('assets\js\users.js') ?>"></script>

    <style>
        /* Loader */
        .loading-overlay {
            display: none;
            background: rgba(255, 255, 255, 0.7);
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            top: 0;
            z-index: 9998;
            align-items: center;
            justify-content: center;
        }

        .loading-overlay.is-active {
            display: flex;
        }
    </style>
</head>

<body>
    <div class="loading-overlay" id="global-loader">
        <span class="fas fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="container">
        <h2>Manage Users</h2>
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="alert alert-success">
                <strong><span class="glyphicon glyphicon-ok"></span> <?php echo $this->session->flashdata('success'); ?></strong>
            </div>
        <?php } ?>
        <div class="" style="text-align: right;">

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add-user-modal" onclick="addUser();">
                Add User
            </button>
        </div>



        <table class="table table-hover" id="users-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Profile</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="tbody">
            </tbody>
        </table>

        <div class="alert alert-info" role="alert" id="no-users-holder">
            <strong>No Users Found!</strong>
        </div>

    </div>
    <!-- Add User Modal -->
    <div class="modal" tabindex="-1" role="dialog" id="add-user-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form role="form" method="post" enctype="multipart/form-data" id="user-form">

                        <input type="hidden" name="x" id="x" size="4" />
                        <input type="hidden" name="y" id="y" size="4" />
                        <input type="hidden" name="x2" id="x2" size="4" />
                        <input type="hidden" name="y2" id="y2" size="4" />
                        <input type="hidden" name="w" id="w" size="4" />
                        <input type="hidden" name="h" id="h" size="4" />

                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" id="name" name="name" maxlength="75">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" id="email" name="email" maxlength="75">
                        </div>
                        <div class="form-group">
                            <label for="img">Profile Image:</label>
                            <input type="file" class="btn btn-primary" id="img" name="img" onchange="readURL(this);">
                        </div>
                        <div class="form-group">
                            <img src="" alt="" id="crop-box">
                        </div>
                        <div class="form-group">
                            <label for="skills">Skills:</label>
                            <select class="form-control" id="skills" name="skills[]" multiple="multiple">
                                <option>PHP</option>
                                <option>Codeigniter</option>
                                <option>Laravel</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="gender">Gender:</label>
                            <input type="radio" name="gender" value="m">&nbsp;Male
                            <input type="radio" name="gender" value="f">&nbsp;Female
                        </div>
                        <div class="form-group">
                            <label for="dob">Dob:</label>
                            <input type="date" max="2018-01-01" class="form-control" id="dob" name="dob">
                        </div>
                        <input type="hidden" id="form-action" name="form-action" value="add">
                        <input type="hidden" id="user_id" name="user_id" value="">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="form-submit-btn">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="modal-close">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
<script>
    const base_url = '<?= base_url() ?>';
</script>