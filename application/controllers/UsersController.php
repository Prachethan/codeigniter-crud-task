<?php


class UsersController extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users');
		$this->load->library('session');
	}

	public function index()
	{
		$data['users'] = $this->users->getAll();
		$this->load->view('users/manage-users', $data);
	}


	public function viewAllUsers()
	{
		$data['users'] = $this->users->getAll();
		header('Content-Type: application/json');
		echo json_encode($data);
		exit;
	}
	public function addUser()
	{
		$data['name'] = $this->input->post('name');
		$data['email'] = $this->input->post('email');
		if (!empty($_FILES['img']['name'])) {
			$data['img'] = $this->doUpload();
		} else {
			$data['img'] = '';
		}
		$skills = $this->input->post('skills') !== NULL
			? implode(',', $this->input->post('skills'))
			: '';

		$data['skills'] = $skills;
		$data['gender'] = $this->input->post('gender');
		$data['dob'] = $this->input->post('dob');
		if ($this->users->insert($data)) {
			$resp['message'] = 'User added Successfully';
			$resp['success'] = TRUE;
		} else {
			$resp['message'] = 'Failed to add the user';
			$resp['success'] = FALSE;
		}
		header('Content-Type: application/json');
		echo json_encode($resp);
		exit;
	}

	public function editUser()
	{
		$users_id = $this->input->post('user_id');
		$users = $this->users->getDataById($users_id);
		$data['name'] = $this->input->post('name');
		$data['email'] = $this->input->post('email');
		if (!empty($_FILES['img']['name'])) {
			$data['img'] = $this->doUpload();
			unlink('./uploads/users/' . $users->img);
		}

		$skills = $this->input->post('skills') !== NULL
			? implode(',', $this->input->post('skills'))
			: '';

		$data['skills'] = $skills;
		$data['gender'] = $this->input->post('gender');
		$data['dob'] = $this->input->post('dob');
		$edit = $this->users->update($users_id, $data);
		if ($edit) {
			$resp['message'] = 'User updated Successfully';
			$resp['success'] = TRUE;
		} else {
			$resp['message'] = 'Failed to update the user';
			$resp['success'] = FALSE;
		}
		header('Content-Type: application/json');
		echo json_encode($resp);
		exit;
	}

	public function viewUser($users_id)
	{

		$data['user'] = $this->users->getDataById($users_id);
		header('Content-Type: application/json');
		echo json_encode($data);
		exit;
	}

	public function deleteUser($users_id)
	{
		$delete = $this->users->delete($users_id);
		if ($delete) {
			$resp['message'] = 'User deleted Successfully';
			$resp['success'] = TRUE;
		} else {
			$resp['message'] = 'Failed to delete the user';
			$resp['success'] = FALSE;
		}
		header('Content-Type: application/json');
		echo json_encode($resp);
		exit;
	}



	function doUpload()
	{
		$folder = './uploads/users/';

		$image = $_FILES['img']['tmp_name'];
		$filename = basename($_FILES['img']['name']);

		$src = imagecreatefrompng($image);
		$tmp = imagecreatetruecolor($this->input->post('w'), $this->input->post('h'));
		imagecopyresampled($tmp, $src, 0, 0, $this->input->post('x'), $this->input->post('y'), $this->input->post('w'), $this->input->post('h'), $this->input->post('w'), $this->input->post('h'));
		imagepng($tmp, $folder . $filename);

		imagedestroy($tmp);
		imagedestroy($src);

		return urlencode($filename);
	}
}
