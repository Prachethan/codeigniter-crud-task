<?php

class Users extends CI_Model
{

    public function getAll()
    {
        return $this->db->get('users')->result_array();
    }

    public function insert($data)
    {
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }

    public function getDataById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('users')->row_array();
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        return true;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('users');
        return true;
    }
}
